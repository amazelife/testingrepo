-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2016 at 07:19 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `demo_search`
--

-- --------------------------------------------------------

--
-- Table structure for table `demo_search1`
--

CREATE TABLE IF NOT EXISTS `demo_search1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `demo_search1`
--

INSERT INTO `demo_search1` (`id`, `title`, `text`) VALUES
(1, 'facebook', 'https://www.facebook.com,\r\nhttps://www.signintofacebook.com\r\nfacebook wikipedia-encycloppdia'),
(4, 'google', 'google search\r\ngoogle search engine\r\ngoogle wikipedia encyclopedia');
